import numpy as np

ds = np.zeros((3046, 421*421))

folder = 'tempfield_r3600/realData'

for i in range(1, 10):
    d = np.loadtxt('tempfield_r3600/realData/data00%d_real' % i)
    print 'data00%d_real' % i
    ds[i-1, :] = d

for i in range(10, 100):
    d = np.loadtxt('tempfield_r3600/realData/data0%d_real' % i)
    print 'data0%d_real' % i
    ds[i-1, :] = d

for i in range(100, 3047):
    d = np.loadtxt('tempfield_r3600/realData/data%d_real' % i)
    print 'data%d_real' % i
    ds[i-1, :] = d



np.save('flowTraj.npy', ds)
