import numpy as np
from functools import partial
from scipy.signal import argrelextrema
from scipy.integrate import solve_ivp
from scipy import optimize
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import sys
import reservoir_fp
reload(reservoir_fp)
from reservoir_fp import Network


"""prediction module

This module contains wrapper functions dealing with the validation and visualization
of reservoir predictions. They provide a convenient way to test different factors
that affect the reservoir prediction quality, such as timestep size, training duration,
post-training delay, etc.
"""

################################################################################
# Good reservoir configs for known systems

lorenzConf = {
    "dr": 300,
    "rho": 1.3,
    "beta": 0.0002,
    "sigma": 0.1,
    "iSeed": 40,
    "aSeed": 40,
    "d": 3
}

# for dt = 0.02
lorenz2 = {
    "dr": 300,
    "rho": 1.251,
    "sigma": 0.023,
    "beta": 7.63e-5,
    "iSeed": 40,
    "aSeed": 40,
    "d": 3
}

# for dt = 0.03
lorenz3 = {
    "dr": 300,
    "rho": 1.1161,
    "sigma": 0.0358,
    "beta": 0.000731,
    "iSeed": 40,
    "aSeed": 40,
    "d": 3
}

lorenz3a = {
    "dr": 300,
    "rho": 1.263,
    "sigma": 0.0314,
    "beta": 0.00144,
    "iSeed": 40,
    "aSeed": 40,
    "d": 3
}
# ~11 time units
lorenz3b = {
    "dr": 300,
    "rho": 1.16,
    "sigma": 0.0348,
    "beta": 0.003643,
    "iSeed": 40,
    "aSeed": 40,
    "d": 3
}

flowConf = {
    "dr": 300,
    "rho": 0.808,
    "sigma": 0.029,
    "beta": 0.001,
    "iSeed": 40,
    "aSeed": 40,
    "d": 42*42
}

flowConf2 = {
    "dr": 300,
    "rho": 1.034,
    "sigma": 0.72,
    "beta": 0.00021,
    "iSeed": 40,
    "aSeed": 40,
    "d": 42*42
}

flowConf3 = {
    "dr": 235,
    "rho": 0.889,
    "sigma": 0.476,
    "beta": 0.00178,
    "iSeed": 40,
    "aSeed": 40,
    "d": 42*42
}

flowConf4 = {
    "dr": 422,
    "rho": 1.392,
    "sigma": 0.034,
    "beta": 0.0674,
    "iSeed": 40,
    "aSeed": 40,
    "d": 42*42
}

flowErr = {
    "dr": 100,
    "rho": 1.14,
    "sigma": 0.7084,
    "beta": 0.02916,
    "iSeed": 40,
    "aSeed": 40,
    "d": 42*42
}




################################################################################
# Comparing prediction with actual trajectory

def subsampleAndPredict(ifplot, traj, ttrain, ttest, conf, dt, sdt):
    """Test the reservoir's prediction.

    Given a trajectory, partition it into training and testing data, then return
    the comparison between the testing data and the reservoir's prediction.

    :param ifplot: Plot or not?
    :returns: Predictions and actual trajectory for comparison
    """

    trained, testing = subsampleAndTrain(traj, ttrain, ttest, conf, dt, sdt)
    testSteps        = testing.shape[0]
    predictions      = trained.predict(testSteps)

    if ifplot:
        ts = np.linspace(0, ttest, testSteps)
        compare(predictions, testing, ts)

    return (predictions, testing)


def skipThenPredict(ifPlot, traj, ttrain, ttest, conf, dt, sdt, forward):
    """Begin predicting at a certain time after training ends.

    Partition the given trajectory, subsample it, construct and train the
    network, then feed it a single data point some time later and start the
    prediction.

    :param forward: Number of steps after which prediction begins
    :returns: Prediction and testing data
    """

    trained, testing = subsampleAndTrain(traj, ttrain, ttest, conf, dt, sdt)

    trained.res_SET(trained.nextRes(testing[forward, :]))
    testing2  = testing[forward+1:, :]
    testSteps = testing2.shape[0]
    V = trained.predict(testSteps)

    if ifPlot:
        ts = np.linspace(dt*forward, ttest, testSteps)
        compare(V, testing2, ts)

    return (V, testing2)


def evolveThenPredict(ifplot, traj, ttrain, ttest, conf, dt, sdt, forward):
    """Begin predicting at a certain time after training ends.

    Partition the given trajectory, subsample it, construct and train the
    network, continue feeding it some more correct data, then start the
    prediction.

    :param forward: Number of steps after which autonomous prediction begins
    :returns: Prediction and testing data
    """

    trained, testing = subsampleAndTrain(traj, ttrain, ttest, conf, dt, sdt)

    testing1 = testing[0:forward+1, :]
    testing2 = testing[forward+1:, :]

    trained.evolve(testing1)
    testSteps = testing2.shape[0]
    V = trained.predict(testSteps)

    if ifplot:
        ts = np.linspace(dt*forward, ttest, testSteps)
        compare(V, testing2, ts)
    else:
        return (V, testing2)


def subsampleAndTrain(traj, ttrain, ttest, conf, dt, sdt):
    """Partition the trajectory, subsample it, the construct and train the network.

    Other similar functions (e.g. subsampleAndPredict) are extensions to this function.

    :param traj: Trajectory; a "trajectory" is a 2D array whose rows represent the states of the system over time
    :param ttrain: Training duration
    :param ttest: Prediction duration
    :param conf: Network configuration
    :param dt: Trajectory's timestep size in time units
    :param sdt: Subsampling timestep size (multiple of dt)
    :returns: Trained network and testing data
    """

    data       = subsample(traj, dt, sdt)
    delaySteps = int(5 / sdt)
    data       = data[delaySteps:, :]

    trainSteps = int(ttrain/sdt)
    testSteps  = int(ttest/sdt)
    training   = data[0:trainSteps, :]
    testing    = data[trainSteps:trainSteps+testSteps, :]

    net = Network(**conf)
    net.train(training)

    return (net, testing)


def subsample(traj, dt, sdt):
    """Subsample a trajectory in multiples of its natural rate."""

    ratio = int(sdt / dt)
    return traj[0::ratio]


################################################################################
# Measuring the quality of the prediction

def maxTime(tf, predict, actual, tolFac=0.1):
    """Return the maximum prediction time.

    The maximum prediction time is the time it takes until
    the difference between the prediction and actual trajectories
    exceeds a given tolerance.

    :param tf: Duration of both trajectories in time units
    :param predict: Predicted 1D trajectory
    :param actual: Actual 1D trajectory
    :param tolFac: Tolerance factor relative to the actual data
    :returns: Maximum prediction time in time units
    """

    predict   = predict[:, 2]
    actual    = actual[:, 2]
    time      = np.linspace(0, tf, predict.shape[0])
    tolerance = tolFac * actual
    err       = np.abs(actual - predict)
    diff      = err - tolerance
    mt        = np.where(diff > 0)
    ind       = mt[0][0]

    return time[ind]

def normOfDiff(tf, predict, actual):
    """Return the norm vector of the difference between two trajectories."""

    err       = predict - actual
    errNorm   = np.linalg.norm(err, axis=1)

    return errNorm

def maxTime_norm(tf, predict, actual, tolFac=0.2):
    """Return the maximum prediction time using normOfDiff as a measurement."""

    errNorm   = normOfDiff(tf, predict, actual)
    time      = np.linspace(0, tf, predict.shape[0])
    tolerance = tolFac * np.linalg.norm(actual, axis=1)
    diff      = errNorm - tolerance
    mt        = np.where(diff > 0)
    ind       = mt[0][0]

    return time[ind]

def meanSE(predict, actual):
    """Return the mean squared error between two vectors."""
    u = (predict - actual) ** 2
    mse = np.mean(u)
    return mse


################################################################################
# Hyperparameter optimization

def randomGS(quantifier, iterations, traj, sdts, rhos, sigmas, betas,
             drs, ttrain, ttest):
    """Random grid-search for hyperparameter optimization.

    Compare different predictions using the given quantifier whose signature is

    (predicted traj, actual traj) -> quality

    The set of parameters that yield the numerically highest quality will be chosen,
    so adjust the quantifier accordingly (see randomGS_meanSE).
    """
    configs = np.zeros((5,))

    for i in range(iterations):
        np.random.seed(i)
        sdt      = 0.01 * np.random.randint(int(sdts[0]/0.01), int(sdts[1]/0.01))
        rho     = np.random.uniform(rhos[0], rhos[1])
        sigma   = np.random.uniform(sigmas[0], sigmas[1])
        beta    = np.random.uniform(betas[0], betas[1])
        dr      = np.random.randint(drs[0], drs[1])
        conf    = {"rho": rho, "sigma": sigma, "beta": beta, "dr": dr,
                   "iSeed": 40, "aSeed": 40, "d": traj.shape[1]}
        V, A    = subsampleAndPredict(False, traj, ttrain, ttest, conf, 0.01, sdt)
        # print [dt, rho, sigma, beta, dr]
        quality = quantifier(predict=V, actual=A)
        configs = np.vstack((configs, np.array([sdt, rho, sigma, beta, dr])))

        if i == 0:
            maxQuality = quality

        if quality > maxQuality:
            maxQuality = quality
            print [quality, sdt, rho, sigma, beta, dr]

    return configs[1:, :]


def randomGS_maxTime(iterations, traj, sdts, rhos, sigmas, betas, drs, ttrain, ttest):
    """Random search based on maximum prediction time. Search ranges are specified
    using tuples.

    :param iterations: How many trials?
    :param traj: The whole trajectory
    :param sdts: Tuple containing [low sdt, high sdt)
    :param rhos: Tuple containing [low spectral radius, high spectral radius)
    :param sigmas: Tuple containing [low sigma, high sigma)
    :param betas: Tuple containing [low beta, high beta)
    :param drs: Tuple containing [low no. of nodes, high no. of nodes)
    :param ttrain: Training duration in time units
    :param ttest: Testing duration in time units
    :returns: Array containing all trial parameter sets as rows
    """

    quantifier = partial(maxTime, tf=ttest)
    return randomGS(quantifier, iterations, traj, sdts, rhos, sigmas, betas, drs, ttrain, ttest)

def randomGS_maxTime_norm(iterations, traj, sdts, rhos, sigmas, betas, drs, ttrain, ttest):
    """Random search based on max time using norm of difference as measurement."""

    quantifier = partial(maxTime_norm, tf=ttest)
    return randomGS(quantifier, iterations, traj, sdts, rhos, sigmas, betas, drs, ttrain, ttest)

def randomGS_meanSE(iterations, traj, sdts, rhos, sigmas, betas, drs, ttrain, ttest):
    """Random search based on mean squared error."""

    quantifier = lambda predict, actual: -meanSE(predict, actual)
    return randomGS(quantifier, iterations, traj, sdts, rhos, sigmas, betas, drs, ttrain, ttest)


################################################################################
# Unused, broken functions


# Find the best set of parameters using brute-force grid search
def gridSearch(predictFunc, measure, dts, rhos, sigmas, betas, ttrain, ttest):
    def f(x, *args):
        dt, rho, sigma, beta = x
        V, A = predictFunc(False, ttrain, ttest,
                           dt=dt, rho=rho, sigma=sigma, beta=beta)
        if measure == "maxTime":
            quality = maxTime(ttest, V[:,2], A[:,2])
        elif measure == "MSE":
            quality = meanSE(V[:,2], A[:,2])
        else:
            sys.exit()

        return -quality

    rranges = (dts, rhos, sigmas, betas)
    results = optimize.brute(f, rranges, full_output=True, finish=None)
    return results


# Vary a single hyperparameter (timestep, spectral radius, input weight range, or
# regularization parameter) while using default values for the rest
# lorenz_r0s = np.load('lorenz_r0s.npy')

def varyHyperParam(param, low, high, dp, ttrain, ttest, iSeed, aSeed, trajs):
    params = np.arange(low, high, dp)
    samples = np.shape(params)[0]
    durations = np.zeros((samples, ))
    numTrajs = trajs.shape[2]

    print "W_in seed = %d" % iSeed
    print "A seed = %d" % aSeed

    for i in range(samples):
        times_over_r0s = np.zeros((numTrajs, ))
        print params[i]

        for j in range(numTrajs):
            traj = trajs[:, :, j]

            if param == "dt":
                V, A = testLorenz(False, ttrain=ttrain, ttest=ttest, dt=params[i],
                                     traj=traj, iSeed=iSeed, aSeed=aSeed)
            elif param == "rho":
                V, A = testLorenz(False, ttrain=ttrain, ttest=test, rho=params[i],
                                     traj=traj, iSeed=iSeed, aSeed=aSeed)
            elif param == "sigma":
                V, A = testLorenz(False, ttrain=ttrain, ttest=test, sigma=params[i],
                                     traj=traj, iSeed=iSeed, aSeed=aSeed)
            elif param == "beta":
                V, A = testLorenz(False, ttrain=ttrain, ttest=test, beta=params[i],
                                     traj=traj, iSeed=iSeed, aSeed=aSeed)
            else:
                print "No hyperparameter chosen"
                return 0

            T = maxTime(ttest, V[:, 2], A[:, 2])
            times_over_r0s[j] = T

        durations[i] = np.mean(times_over_r0s)

    return (params, durations)


# Compare the maximum prediction time of different Lorenz trajectories
def varyInitCond(samples, ttrain, ttest):
    durations = np.zeros((samples, ))
    # inits = np.zeros((samples, 3))
    lim = 10
    coors = np.random.uniform(1, lim+1, (samples, 3))

    for i in range(samples):
        x0 = coors[i, 0]
        y0 = coors[i, 1]
        z0 = coors[i, 2]
        r0 = (x0, y0, z0)
        # inits[i] = r0
        V, A = predictLorenz(False, ttrain, ttest, r0=r0)
        T = maxTime(ttest, V[:, 2], A[:, 2])
        durations[i] = T
        # mse = meanSE(V, A)
        # errors[i] = mse

    return durations


# Compare the maximum prediction times of different randomized weights
# Should be plotted using a histogram
def varyRandomSeed(mode, samples, ttrain, ttest):
    durations = np.zeros((samples,))

    for i in range(samples):
        if mode == "input":
            V, A = predictLorenz(False, ttrain, ttest, iSeed=i)
        else:
            V, A = predictLorenz(False, ttrain, ttest, aSeed=i)

        t = np.linspace(0, ttest, V.shape[0])
        T = maxTime(ttest, V[:,2], A[:,2])
        durations[i] = T

    return durations


#######################################################################


# General comparison plot up to 3D data
def compare(predict, actual, t, fontsize=10):
    d = np.shape(predict)[1]
    plt.clf()
    plt.ion()
    plt.subplot(311)
    plt.plot(t, actual[:, 0])
    plt.plot(t, predict[:, 0])
    plt.ylabel('x', fontsize=fontsize)
    ax = plt.gca()
    #ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)

    if d >= 2:
        plt.subplot(312)
        plt.plot(t, actual[:, 1])
        plt.plot(t, predict[:, 1])
        plt.ylabel('y', fontsize=fontsize)
        ax = plt.gca()
        #ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
        plt.xticks(fontsize=fontsize)
        plt.yticks(fontsize=fontsize)

    if d >= 3:
        plt.subplot(313)
        plt.plot(t, actual[:, 2])
        plt.plot(t, predict[:, 2])
        plt.ylabel('z', fontsize=fontsize)
        ax = plt.gca()
        #ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
        plt.xticks(fontsize=fontsize)
        plt.yticks(fontsize=fontsize)

    plt.xlabel('Time', fontsize=fontsize)

    plt.show()


def returnMap(predict, actual):
    p = localMax(predict)
    a = localMax(actual)

    p1 = p[0:-1]
    p2 = p[1:]

    a1 = a[0:-1]
    a2 = a[1:]

    plt.ion()
    plt.clf()
    plt.scatter(a1, a2)
    plt.scatter(p1, p2)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.xlabel('$z_i$', fontsize=20)
    plt.ylabel('$z_{i+1}$', fontsize=20)
    plt.show()


def localMax(xs):
    return xs[argrelextrema(xs, np.greater)[0]]


def plotHist(x, bins, title, xlabel, ylabel):
    plt.clf()
    plt.hist(x, bins)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.xlabel(xlabel, fontsize=20)
    plt.ylabel(ylabel, fontsize=20)
    plt.title(title, fontsize=25)
    plt.show()


def plotXY(x, y, title='test', xlabel='x', ylabel='y'):
    plt.ion()
    plt.clf()
    plt.plot(x, y)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.xlabel(xlabel, fontsize=20)
    plt.ylabel(ylabel, fontsize=20)
    plt.title(title, fontsize=25)
    plt.show()
