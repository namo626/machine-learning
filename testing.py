# testing.py - loads the testing data
# whenever functions are updated, load the corresponding modules instead

import prediction
reload(prediction)
from prediction import *
import data
reload(data)
from data import *

times, testLorenz = generateLorenz((1,1,1), 500, 0.01, 10, 28, 8.0/3)
traj = np.load('flowTraj_42x42.npy')
