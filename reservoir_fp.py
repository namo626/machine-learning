import numpy as np
from scipy import sparse as sp
from scipy import optimize as op
from math import ceil

import initialize
reload(initialize)
from initialize import mkInputWs_dense, mkInputWs_sparse, mkResWs_dense, mkResWs_sparse


class Network:
    def __init__(self, d, dr, rho, sigma, beta, iSeed, aSeed, isSparse=False):

        # self.isSparse = (d > 100*100)
        self.isSparse = isSparse

        if self.isSparse:
            # input weights
            self.W_in = mkInputWs_sparse(dr, d, sigma, iSeed, density=0.01)
            # reservoir weights
            self.A    = mkResWs_sparse(dr, rho, aSeed, density=0.01)
        else:
            self.W_in = mkInputWs_dense(dr, d, sigma, iSeed)
            self.A    = mkResWs_dense(dr, rho, aSeed, density=0.01)

        # reservoir nodes (states)
        self.r    = np.zeros((dr, ))
        # readout matrix
        self.P    = 1
        # dimension of input
        self.d    = d
        # number of nodes
        self.dr   = dr
        # regularization parameter
        self.beta = beta


    # Pure functions

    def nextRes(self, nextInput):
        """Return the next reservoir state given a reservoir and an input vector.

        :param nextInput: Next input from the system
        :returns: Updated reservoir state
        """

        r_next = np.tanh(self.A.dot(self.r) + self.W_in.dot(nextInput))

        return r_next


    def readout(self):
        """Generate an output vector based on the reservoir's current state.

        :returns: Output vector in system space
        """

        return self.P.dot(self.r)


    # Functions with side-effects

    def train(self, trainingInput, t0=0.04):
        """Train a network using the given training inputs.

        Side effects:

        * The network's reservoir state and P matrix are updated.

        :param trainingInput: Training inputs
        :param t0: Washout time as a fraction of the length of trainingInput
        """

        steps, dimension = np.shape(trainingInput)
        washout = int(t0 * steps)

        # holds the reservoir states as columns
        R = np.zeros((self.dr, steps-washout))
        # holds the training data after washout time
        U = trainingInput[washout:, :]

        for i in range(steps):
            if i >= washout:
                R[:, i-washout] = self.r

            self.r = self.nextRes(trainingInput[i, :])

        # optimize the readout
        if self.isSparse:
            R = sp.csr_matrix(R)
            U = sp.csr_matrix(U)
            self.P = linearReg_sparse(R, U, self.beta)
        else:
            self.P = linearReg_dense(R, U, self.beta)


    def evolve(self, trainingInput):
        """Evolve the reservoir state without optimizing the readout.

        Side effects:
        * The reservoir state is updated

        :param trainingInput: Trajectory data to feed the reservoir
        """

        steps, dim = np.shape(trainingInput)
        for i in range(steps):
            self.r = self.nextRes(trainingInput[i, :])


    def predict(self, steps):
        """Return the prediction results from a given reservoir.

        Side effects:

        * The reservoir state is updated at every timestep

        :param steps: Number of steps to predict
        :returns: steps x d matrix containing the predictions
        """
        # prediction vector
        V = np.zeros((steps, self.d))

        for i in range(steps):
            V[i, :] = self.readout()
            self.r = self.nextRes(V[i, :])

        return V


    def res_SET(self, resState):
        """Manually assign a new reservoir state.

        Side effects:

        * The reservoir state is updated

        :param resState: New reservoir state.
        """
        self.r = resState



def linearReg_dense(R, U, beta):
    """Return an optimized matrix using Tikhonov regularization.

    :param R: Past reservoir states in columns (matrix-like)
    :param U: Past input vectors in columns (matrix-like)
    :param beta: Regularization parameter
    :returns: Optimized readout matrix (matrix-like)
    """
    # each column is one time step, like R
    Ut = U.transpose()
    X = np.copy(R)
    # X[1::2, :] = X[1::2, :] ** 2
    P_new = np.dot(np.dot(Ut, X.transpose()),
                   np.linalg.inv(np.dot(X, X.transpose())
                                 + beta * np.identity(X.shape[0])))

    return P_new

def linearReg_sparse(R, U, beta):
    """Return an optimized (sparse) matrix using Tikhonov regularization."""

    Ut = U.transpose().tocsr()
    X  = R.copy()
    Xt = X.transpose().tocsr()
    P_new = sp.csr_matrix.dot(sp.csr_matrix.dot(Ut, Xt),
                              sp.linalg.inv(X.dot(Xt)
                                            + sp.identity(X.shape[0]).multiply(beta)))
    return P_new
