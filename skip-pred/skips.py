# Keeping the trajectory and training duration constant, start prediction at different times for different initial conditions

import sys
sys.path.append('/home/namo/Research/machine-learning/src')

import matplotlib.pyplot as plt
from prediction import *

# Trajectories with random initial conditions
lorenzs = np.load('../lorenz_r0s.npy')
r0s = 500
lorenzs = lorenzs[:, :, 0:r0s]

# When to start prediction (skipping in steps, not time)
skips = np.arange(10, 200, 10)
ttrain = 100
ttest = 25
dt = 0.07

results = np.zeros((r0s, skips.shape[0]))
for j in range(lorenzs.shape[2]):

    trajectory = lorenzs[:, :, j]
    mts = np.zeros(skips.shape)

    for i in range(skips.shape[0]):
        forward = skips[i]
        V, A = jumpPredict(False, trajectory, ttrain, ttest, lorenzConf, dt, forward)
        # plt.subplot(311)
        # plt.title('r%d, skip %d steps, dt = %.2f' % (j, forward, dt))
        # plt.savefig('r%d_skip%d_%.2fdt.png' % (j, forward, dt), dpi=200)
        mt = maxTime(ttest-(dt*forward), V[:, 2], A[:, 2], tolFac=0.5)
        mts[i] = mt

    results[j, :] = mts
    # print results[j, :]

mean_results = np.mean(results, axis=0)
np.savetxt('results_r0s.csv', zip(skips, mean_results))
