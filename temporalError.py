from testing import *

confs = configs[:100, :]
ttrain = 1
ttest = 3
errs = []

for i in range(confs.shape[0]):
    conf = confs[i]
    flowConf = {
        "dr": int(conf[4]),
        "rho": conf[1],
        "sigma": conf[2],
        "beta": conf[3],
        "iSeed": 40,
        "aSeed": 40,
        "d": 42*42
    }

    ttrain = ttrains[j]
    preds, testing = subsampleAndPredict(False, traj, ttrain, ttest, flowConf, 0.01)
    time, err = error2D(ttest, preds, testing)
    errs.append(err)

print "Done"
