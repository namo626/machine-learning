# main.py - load the actual numerical data

import prediction
reload(prediction)
from prediction import *
import os

thisDir = os.path.dirname(__file__)

lorenz = np.loadtxt(os.path.join(thisDir, 'dataLorenz.csv'))
