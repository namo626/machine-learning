
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation

traj1 = preds
traj2 = testing

fig, (ax1, ax2) = plt.subplots(1,2)
im1 = ax1.imshow(traj1[0,:].reshape(42, 42), cmap='jet')
im2 = ax2.imshow(traj2[0,:].reshape(42, 42), cmap='jet')
title1 = ax1.set_title("Reservoir: Frame 1", fontsize=15)
title2 = ax2.set_title("Real Data: Frame 1", fontsize=15)

def animate(i):
    z = traj1[i, :].reshape(42, 42)
    a = traj2[i, :].reshape(42, 42)
    im1.set_array(z)
    im2.set_array(a)
    title1.set_text("Reservoir: Frame %d" % i)
    title2.set_text("Real Data: Frame %d" % i)

    return [im1, im2]

anim = animation.FuncAnimation(fig, animate,
                               frames=range(traj1.shape[0]),
                               interval=50,
                               repeat=False)

anim.save('flow2D_movie.mp4', fps=30, dpi=150, extra_args=['-vcodec', 'libx264'])

plt.show()
