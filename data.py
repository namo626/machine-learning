import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D
import time

# This file contains the data generators

def RK4(f, r0, tf, dt):
    """Fourth-order Runge-Kutta integrator.

    :param f: Function to be integrated
    :param r0: Initial conditions
    :param tf: Integration duration
    :param dt: Timestep size
    :returns: time and trajectory vectors

    """

    ts = np.arange(0, tf, dt)
    sol = np.zeros((ts.shape[0], len(r0)))
    sol[0, :] = np.array(r0)

    for i in range(0, ts.shape[0]-1):
        t = ts[i]
        r = sol[i, :]

        k1 = dt * f(r, t)
        k2 = dt * f(r + k1/2, t + dt/2)
        k3 = dt * f(r + k2/2, t + dt/2)
        k4 = dt * f(r + k3, t + dt)
        K = (1.0/6)*(k1 + 2*k2 + 2*k3 + k4)

        sol[i+1, :] = r + K

    return (ts, sol)


def generateLorenz(r0, tf, dt, sigma, rho, beta):
    """Integrate a given Lorenz system."""

    def lorenz(r, t):
        x = r[0]; y = r[1]; z = r[2]
        u = sigma * (y - x)
        v = x * (rho - z) - y
        w = x * y - beta * z
        return np.array([u, v, w])

    ts, sol = RK4(lorenz, r0, tf, dt)
    return (ts, sol)


def generateSine(tf, dt):
    x = np.arange(0, tf, dt)
    x = x.reshape((len(x), 1))
    return 100*np.sin(x)



# Visualization functions

def poincare(rs, axis, val, lw=2, s=300):
    plt.clf()
    plt.ion()
    tolerance = 0.1
    xs = rs[:,0]; ys = rs[:,1]; zs = rs[:,2]

    if axis == "z":
        mask = np.abs(zs - val) < tolerance
        ts = np.arange(1, np.sum(mask))
        cs = plt.cm.hsv(ts)
        plt.scatter(xs[mask], ys[mask], lw=lw, s=s, facecolors='none', edgecolors=cs)
    elif axis == "x":
        mask = np.abs(xs - val) < tolerance
        ts = np.arange(1, np.sum(mask))
        cs = plt.cm.hsv(ts)
        plt.scatter(ys[mask], zs[mask], lw=lw, s=s, facecolors='none', edgecolors=cs)
    else:
        mask = np.abs(ys - val) < tolerance
        ts = np.arange(1, np.sum(mask))
        cs = plt.cm.hsv(ts)
        plt.scatter(xs[mask], zs[mask], lw=lw, s=s, facecolors='none', edgecolors=cs)


def poincareDot(rs, axis, val):
    plt.ion()
    tolerance = 0.1
    xs = rs[:,0]; ys = rs[:,1]; zs = rs[:,2]

    if axis == "z":
        mask = np.abs(zs - val) < tolerance
        ts = np.arange(np.sum(mask))
        cs = plt.cm.hsv(ts)
        plt.scatter(xs[mask], ys[mask], c=cs)
    elif axis == "x":
        mask = np.abs(xs - val) < tolerance
        ts = np.arange(np.sum(mask))
        plt.scatter(ys[mask], zs[mask], c=cs)
    else:
        mask = np.abs(ys - val) < tolerance
        ts = np.arange(np.sum(mask))
        plt.scatter(xs[mask], zs[mask], c=cs)


def scatter3D(rs):
    """Produce a 3D scatter plot of the given 3D trajectory."""

    xs = rs[:,0]; ys = rs[:,1]; zs = rs[:,2]
    fig = plt.gcf()
    plt.clf()
    plt.ion()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(xs, ys, zs)


def plot3D(rs):
    """Produce a 3D line plot of the given 3D trajectory."""

    xs = rs[:,0]; ys = rs[:,1]; zs = rs[:,2]
    fig = plt.gcf()
    plt.clf()
    plt.ion()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(xs, ys, zs)



# Dealing with 2D data

def readImg(filepath, dim):
    """Convert image file to (square) matrix."""

    ds = np.loadtxt(filepath)
    return ds[:, 3].reshape(dim, dim)


def cropCenter(img, xs, ys):
    """Select a rectangular region from the center of the image."""

    y, x = img.shape
    startx = x//2 - (xs//2)
    starty = y//2 - (ys//2)
    return img[starty:starty+ys, startx:startx+xs]


def cropSeries(imgs, dim, xs, ys):
    """Apply cropCenter to a series of square images (each as 1D vector)
    and convert them back to 1D."""

    snapshots = imgs.shape[0]
    series = np.zeros((snapshots, xs*ys))

    for i in range(snapshots):
        img          = imgs[i, :]
        img2D        = np.reshape(img, (dim, dim))
        sqr          = cropCenter(img2D, xs, ys).flatten()
        series[i, :] = sqr

    return series


def plotImg(vec, dim, i):
    """Plot an square image from a 1D vector."""
    plt.clf()
    plt.ion()
    plt.title('Frame %d' % i)
    plt.imshow(vec.reshape(dim, dim))
    plt.show()
