import networkx as nx
import numpy as np
import scipy.sparse as sp


###############################################################################
# Functions to construct the weights


def mkInputWs_sparse(dr, d, sigma, s, density):
    """Sparse version"""

    def f(n):
        np.random.seed(s)
        return np.random.uniform(-sigma, sigma, (n,))

    w_in = sp.random(dr, d, density=density, format='csr', data_rvs=f)
    return w_in

def mkInputWs_dense(dr, d, sigma, s):
    """Construct the input weights (W_in).

    :param dr: Number of nodes
    :param d: Dimension of system
    :param sigma: Range of nonzero values
    :param s: Random seed
    :returns: (dr x d) matrix
    """

    np.random.seed(s)
    return np.random.uniform(-sigma, sigma, (dr, d))

def mkResWs_sparse(n, specRad, aSeed, density):
    """Sparse version"""

    def f(l):
        np.random.seed(aSeed)
        return np.random.uniform(-1, 1, (l,))

    R = sp.random(n, n, density=density, format='csr', data_rvs=f)
    return scaleRho(R, specRad)

def mkResWs_dense(n, specRad, aSeed, density):
    """Construct the reservoir weights (A).

    * for n = 600, p of 0.005 gives avg degree of 6
    * for n = 300, p of 0.01 gives avg degree of 6

    :param n: Number of nodes
    :param p: Probability that a node will be constructed
    :param specRad: Desired spectral radius
    :param aSeed: Random seed
    :returns: (n x n) matrix containing reservoir weights
    """

    G = nx.erdos_renyi_graph(n, density, seed=aSeed, directed=True)
    # print 'Average degree of A: %.2f' % meanDegree(G)

    # adjust the nonzero elments according to rho
    H = nx.to_numpy_array(G)
    H = H * np.random.uniform(-1, 1, np.shape(H))

    A = scaleRho(H, specRad)

    # print 'Spectral radius of A: %.4f' % largestEig(A)
    return A


###############################################################################
# Auxiliary functions


# From Jaeger's paper on ESN
def scaleRho(M, rho):
    """Return the matrix with the desired spectral radius."""

    W1 = M / largestEig(M)
    A = rho * W1
    return A


def largestEig(A):
    """Return the largest eigenvalue of the matrix."""
    if True:
        w, v = np.linalg.eig(A)
    else:
        w, v = sp.linalg.eigs(A)

    return np.max(np.abs(w))


def meanDegree(G):
    """Return the average degree of a graph."""
    ls = list(G.degree())
    ds = [d for n, d in ls]

    return np.mean(ds)
