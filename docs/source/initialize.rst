initialize module
*****************

Weight generators
=================

.. automodule:: initialize
    :members: mkInputWs_dense, mkInputWs_sparse, mkResWs_dense, mkResWs_sparse


Auxiliary functions
===================
.. automodule:: initialize
    :members: scaleRho, largestEig, meanDegree
