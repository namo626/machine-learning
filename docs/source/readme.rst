Usage
*****

Here, a "trajectory" must be a 2D array where each row represents a snapshot of the system at a particular time.

Example 1: Simple prediction
============================

This example uses the reservoir to predict the Lorenz system trajectory. First import the reservoir class, Lorenz trajectory data, and sample network configuration (a dictionary).

   >>> from reservoir_fp import Network
   >>> from prediction import lorenzConf
   >>> from data import generateLorenz, plot3D

Generate the Lorenz trajectory for 500 time units:

  >>> _, dataLorenz = generateLorenz((1,1,1), 500, 0.01, 10, 28, 8.0/3)

Then construct and train the network, using the trajectory as training data:

   >>> network = Network(**lorenzConf)
   >>> network.train(dataLorenz)

Finally, generate the prediction time series for 100 steps after the training ends:

  >>> V = network.predict(100)
  >>> plot3D(V)

Output plot of V:

.. image:: ../../sample_V.png


Example 2: Comparing prediction with actual trajectory
======================================================

This example again uses the Lorenz system, but we will partition the trajectory data into 2 parts: one for training and one for comparing with the reservoir's prediction.

Instead of directly manipulating the Network object, we will use the wrapper function from the module prediction. This module contains wrapper functions for manipulating/testing the reservoir predictions:

  >>> from prediction import subsampleAndPredict, lorenzConf

Then specify the training duration, prediction duration, and the timestep size that will be used to subsample the trajectory:

  >>> predicted, actual = subsampleAndPredict(ifPlot=True,
                                              traj=dataLorenz,
                                              ttrain=100,
                                              ttest=25,
                                              conf=lorenzConf,
                                              dt=0.03)

Output plot:

.. image:: ../../dt_0.08.png


Example 3: Predicing 2D data
=================================

Here we apply the reservoir computer to 2D image data. First we load and subsample the images,
using only the 42x42 pixels region of each image:

  >>> from data import *
  >>> from prediction import flowConf
  >>> subImgs = cropSeries(imgs, 421, 42, 42)

Then train and predict as in example 2, but without plotting:

  >>> predicted, actual = subsampleAndPredict(False, subImgs, 1, 3, flowConf, dt=0.01)

Then run the animation script (for some reason this doesn't work when defined as a function,
so you will have the adjust the variable names accordingly.)
