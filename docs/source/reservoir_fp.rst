reservoir\_fp module
********************

Note that the sparse variation of the network hasn't been fully tested.

.. automodule:: reservoir_fp

Reservoir functions
===================

.. autoclass:: Network
   :members:

Misc.
=====

.. automodule:: reservoir_fp
   :members:
