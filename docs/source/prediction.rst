prediction module
*****************

.. automodule:: prediction

Functions to test the reservoir's predictions
=============================================

.. automodule:: prediction
    :members: subsampleAndTrain, subsampleAndPredict, skipThenPredict, evolveThenPredict


Quantifying the prediction quality
==================================

Here a quantifier refers to a function that calculates numerically how well the prediction compares to the actual data.

.. automodule:: prediction
    :members: maxTime, meanSE, maxTime2D, error2D

Hyperparameter optimization
===========================

.. automodule:: prediction
    :members: randomGS_maxTime, randomGS_meanSE, randomGS_maxTime2D
