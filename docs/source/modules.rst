src
===

.. toctree::
   :maxdepth: 4

   conf
   data
   initialize
   main
   prediction
   reservoir
   reservoir_fp
   testing
   vary-seed-dt
