data module
***********

Trajectory generators
=====================

.. automodule:: data
    :members: RK4, generateLorenz

Visualization functions
=======================

.. automodule:: data
    :members: scatter3D, plot3D, plotImg

Handling 2D data
==================

.. automodule:: data
   :members: cropCenter, cropSeries, readImg
