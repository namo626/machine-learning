.. reservoir documentation master file, created by
   sphinx-quickstart on Mon Jul 30 14:02:14 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for reservoir
===========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   readme


.. toctree::
   :maxdepth: 1
   :caption: Modules:

   reservoir_fp
   prediction
   initialize
   data



* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
