Overview
********

This is a python implementation of the UMD group's reservoir computer.

Here is an overview of the modules in this package:

* reservoir_fp - the core implementation of the reservoir computer
* prediction   - contains functions that test and visualize the predictions made by the reservoir
* data         - contains some trajectory generators and plotting functions
* initialize   - contains the constructors for the network weights

Sample workflow:

1. Load the trajectory data, e.g. Lorenz externally.
2. Use functions in the prediction module to train and test on that trajectory.
3. Vary the network configuration and repeat step 2.

More details are shown in the Usage section.
