from testing import *

confs = configs[:100, :]
ttrains = np.arange(0.5, 5.5, 0.5)
ttest = 3
times = np.zeros((confs.shape[0], ttrains.shape[0]))

for i in range(confs.shape[0]):
    conf = confs[i]
    flowConf = {
        "dr": int(conf[4]),
        "rho": conf[1],
        "sigma": conf[2],
        "beta": conf[3],
        "iSeed": 40,
        "aSeed": 40,
        "d": 42*42
    }

    for j in range(ttrains.shape[0]):
        ttrain = ttrains[j]
        preds, testing = subsampleAndPredict(False, traj, ttrain, ttest, flowConf, 0.01)
        times[i, j] = maxTime2D(ttest, preds, testing)

print "Done"
